------------------------------------------------------------------------------
--                                                                          --
--                        MINIMAL POSTGRESQL BINDING                        --
--                                                                          --
--                                M I N P Q                                 --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2009 Minerva Development Team                                  --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for a semi-thick binding to libPQ.  --
-- Not all functionality of libPQ is made available by this binding - it    --
-- started out as what was necessary for the operation of the MUD Server    --
-- Minerva.  Some other functionality has been added since to support other --
-- applications.                                                            --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C.Strings;
with Ada.Strings.Unbounded;
with Ada.Finalization;
with Ada.Calendar;

package MinPQ is
    type Connection is tagged private;
    type Result is tagged private;

    -- These Statuses are the currently known ones returned by a server upon
    -- connecting (or attempting to connect.)
    type Status_Type is (
        Connection_OK,
        Connection_Bad,
        Unknown_Status
    );

    -- These are the possible statuses returned by the server after executing
    -- a query.
    type Exec_Status_Type is (
        Empty_Query,
        Command_OK,
        Tuples_OK,
        Copy_Out,
        Copy_In,
        Bad_Response,
        Nonfatal_Error,
        Fatal_Error,
        Unknown_Result_Status
    );

    -- These are the possible data types from the database exported by this
    -- binding.
    type Data_Type is (
        SQL_Character,
        SQL_Varchar,
        PG_Text,
        SQL_Smallint,
        SQL_Integer,
        SQL_Bigint,
        SQL_Real,
        SQL_Double,
        SQL_Decimal,
        SQL_Boolean,
        SQL_Time,
        SQL_Date,
        SQL_Timestamp
    );

    -- The exceptions that can be raised by this binding.
    Fatal_Connection_Error, Nonfatal_Connection_Error : exception;
    Fatal_Query_Error, Nonfatal_Query_Error : exception;

    -- These types are for passing parameters to SQL statements.  Only
    -- parameterized statements are supported in this binding, to minimize the
    -- possibility of SQL injection.
    type Parameter is private;
    type Param_Array is array(Positive range <>) of Parameter;
    Empty_Param_Array : constant Param_Array(1 .. 0);

    -- Paramters are what they sound like.  User, Password, and Database must
    -- all contain only characters in the BMP.  If Host is the empty string, the
    -- default is assumed (either a UNIX domain socket or, if on a host without
    -- UNIX socket support, localhost.)  Port is not used if the host is not
    -- specified.  For any other Connection Conn_Prime such that
    -- Same_Connection(Conn, Conn_Prime) before the call to this procedure was
    -- made, Conn_Prime will remain unchanged and it will no longer be the case
    -- that Same_Connection(Conn, Conn_Prime). Raises Fatal_Connection_Error if
    -- the connection attempt was unsuccessful for any reason.
    procedure Open(
        Conn : in out Connection;
        User : in Wide_String;
        Password : in Wide_String;
        Database : in Wide_String;
        Host : in String := "";
        Port : in Natural := 5432);

    -- Returns True iff Conn is open.
    function Is_Open(Conn : Connection) return Boolean;

    -- If Conn.Is_Open, a new connection to the database is made using Conn's
    -- parameters and returned.  If not Conn.Is_Open, a new un-opened connection
    -- is returned.
    function Clone(Conn : Connection) return Connection;

    -- Returns True iff Left and Right are connected to the same database using
    -- the same parameters.
    function "="(Left, Right : Connection) return Boolean;

    -- Returns True iff Left and Right are in fact the same connection to a
    -- database.
    function Same_Connection(Left, Right : Connection) return Boolean;

    -- Returns a server-supplied error message if something goes wrong with the
    -- connection.  If everything is fine with the connection, the empty string
    -- is returned.  This function is used internally - mainly when raising
    -- exceptions - it should rarely need to be called outside this package.
    function Error_Message(Conn : Connection) return String;

    -- Returns the status of the connection.  As with Error_Message, this should
    -- rarely be called outside of this package.
    function Status(Conn : Connection) return Status_Type;

    -- Executes 'Command' with 'Parameters' on connection 'Conn'.  Result_Format
    -- specifies whether the results should be returned in text or binary
    -- format.  Default (0) is text.  This should rarely need to be changed.
    -- Can raise the following exceptions:
    -- Fatal_Connection_Error: if 'Conn' is not an open connection.
    -- Nonfatal_Query_Error: if execution of an empty query is attempted, or if
    --   the server sets the Result's status to Nonfatal_Error
    -- Fatal_Query_Error: if the server delivers an unintelligible response, if
    --   it sets the status to Fatal_Error, or if it sets the status to
    --   an Unknown_Status (probably indicative of a bug in this package).
    function Execute_Query(
        Conn : Connection'Class;
        Command : Wide_Wide_String;
        Parameters : Param_Array := Empty_Param_Array;
        Result_Format : Natural := 0) return Result;

    -- This is simply a convenience function provided for Queries where no
    -- result is expected.  It simply calls the function above, but discards
    -- the result.
    procedure Execute_Query(
        Conn : Connection;
        Command : Wide_Wide_String;
        Parameters : Param_Array := Empty_Param_Array);

    -- Returns the status of a query after execution.  As with the previous
    -- error functions, this should rarely need to be called out of this
    -- package.
    function Status(Res : Result) return Exec_Status_Type;

    -- Returns the number of rows in Result.
    function Rows(Res : Result) return Natural;

    -- Returns the number of columns in Result.
    function Columns(Res : Result) return Natural;

    -- Returns the string name of Column in Res.  Will raise
    -- Nonfatal_Query_Error if Column is out of bounds.
    function Column_Name(
        Res : Result;
        Column : Natural) return Wide_Wide_String;

    -- Returns the type of Column in Res.  Will raise Nonfatal_Query_Error if
    -- Column is out of bounds.
    function Column_Type(Res : Result; Column : Natural) return Data_Type;

    -- Returns True iff Identifier is a valid PostgreSQL identifier (A valid
    -- PostgreSQL identifier is a valid SQL identifier, but can also contain
    -- $ as any character but the first)
    -- NOTE: This function should NOT be used to escape parameters.  It is only
    -- for checking to make sure table and column names are valid when they
    -- cannot be part of the literal query string, e.g.
    --
    -- "SELECT " & Column & " FROM " & Table "WHERE Some_Value=$1".
    --
    -- Notice that the Value for Some_Value is still treated as a parameter.
    function Valid_Identifier(Identifier : Wide_Wide_String) return Boolean;

    --------------------------------------------------------------------------
    -- The descriptions of the following Paramaterize and Get functions are --
    -- meant to describe not only the string versions, but all following    --
    -- versions for all types.                                              --
    --------------------------------------------------------------------------

    -- The Parameterize functions take their arguments and return an object to
    -- be used in a list of parameters to a query.
    -- NOTE: A value of a modular type larger than 2**63-1 will cause a
    -- Constraint_Error to be raised, due to the fact that PostgreSQL supports
    -- integers up to only 2**63-1.
    function Parameterize(Item : Wide_Wide_String) return Parameter;
    function Parameterize(Item : Wide_Wide_String) return Param_Array;

    -- Returns the value of the item in Row 'Row', Column 'Column'.  If either
    -- 'Row' or 'Column' are out of bounds, it will raise Nonfatal_Query_Error
    -- (with an indication that an out of bounds error occurred.)  Will raise
    -- Fatal_Query_Error if Result is uninitialized or cleared.
    -- For non-string types, Constraint_Error will be raised if the value in
    -- the database is incompatible with the desired type.
    function Get(
        Res : Result;
        Row : Natural;
        Column : Natural) return Wide_Wide_String;

    -- This is an overloaded function provided for convenience.  Returns the
    -- value of the item in Row 'Row', in the Column named 'Column'.  If either
    -- 'Row' is out of bounds or 'Column' does not actually name a column of
    -- the table these results come from, it will raise Nonfatal_Query_Error
    -- (again, indicating out of bounds.)  It will raise Fatal_Query_Error if
    -- Result is uninitialized or cleared.
    -- For non-string types, Constraint_Error will be raised if the value in
    -- the database is incompatible with the desired type.
    function Get(
        Res : Result;
        Row : Natural;
        Column : Wide_Wide_String) return Wide_Wide_String;

    -- NOTE: An instantiation of this package for standard type Integer has
    -- been made at MinPQ.Integer_IO for convenience.
    generic
        type Num is range <>;
    package Integer_Type_IO is
        function Parameterize(Item : Num) return Parameter;
        function Parameterize(Item : Num) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Num;
    end Integer_Type_IO;

    generic
        type Num is mod <>;
    package Modular_IO is
        function Parameterize(Item : Num) return Parameter;
        function Parameterize(Item : Num) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Num;
    end Modular_IO;

    -- NOTE: An instantiation of this package for (semi-)standard type
    -- Long_Float has been made at MinPQ.Long_Float_IO for convenience.
    generic
        type Num is digits <>;
    package Floating_Point_IO is
        function Parameterize(Item : Num) return Parameter;
        function Parameterize(Item : Num) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Num;
    end Floating_Point_IO;

    generic
        type Num is delta <>;
    package Fixed_IO is
        function Parameterize(Item : Num) return Parameter;
        function Parameterize(Item : Num) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Num;
    end Fixed_IO;

    generic
        type Num is delta <> digits <>;
    package Decimal_IO is
        function Parameterize(Item : Num) return Parameter;
        function Parameterize(Item : Num) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Num;
    end Decimal_IO;

    generic
        type Enum is (<>);
    package Enumeration_IO is
        function Parameterize(Item : Enum) return Parameter;
        function Parameterize(Item : Enum) return Param_Array;

        function Get(Res : Result; Row : Natural; Column : Natural) return Enum;
        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Enum;
    end Enumeration_IO;

    package Boolean_IO is
        function Parameterize(Item : Boolean) return Parameter;
        function Parameterize(Item : Boolean) return Param_Array;

        function Get(
            Res : Result;
            Row : Natural;
            Column : Natural) return Boolean;

        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Boolean;
    end Boolean_IO;

    -- NOTE: This package is for use with the timestamp with timezone type.  If
    -- your query did not specify a timezone, then timestamps are
    -- retrieved/entered in the system's local time.  This package should
    -- theoretically work (it has not been tested) with timestamps without
    -- timezone info should you be in a rare situation where such a timestamp
    -- is useful.
    -- NOTE: Currently, only second-level accuracy is supported with this
    -- package.
    package Time_IO is
        function Parameterize(Item : Ada.Calendar.Time) return Parameter;
        function Parameterize(Item : Ada.Calendar.Time) return Param_Array;

        function Get(
            Res : Result;
            Row : Natural;
            Column : Natural) return Ada.Calendar.Time;

        function Get(
            Res : Result;
            Row : Natural;
            Column : Wide_Wide_String) return Ada.Calendar.Time;
    end Time_IO;

    -- Closes Conn.  For any other Connection Conn_Prime if
    -- Same_Connection(Conn, Conn_Prime), then Conn_Prime will also be closed
    -- when this procedure returns.
    procedure Close(Conn : in out Connection);
private
    type PG_Conn_Internal is null record;
    type PG_Conn_Ptr is access PG_Conn_Internal;

    type PG_Connection_Counter is limited record
        PG_Conn : PG_Conn_Ptr;
        Count : Natural := 1;
        Active : Boolean := False;
    end record;
    type PG_Conn_Counter_Ptr is access PG_Connection_Counter;

    type Connection is new Ada.Finalization.Controlled with record
        PG_Data : PG_Conn_Counter_Ptr := null;
        Connect_String : Ada.Strings.Unbounded.String_Access := null;
    end record;
    procedure Open(Conn : in out Connection; Connect_String : in String);

    procedure Initialize(Object : in out Connection);
    procedure Adjust(Object : in out Connection);
    procedure Finalize(Object : in out Connection);

    type PG_Result_Internal is null record;
    type PG_Result_Ptr is access PG_Result_Internal;

    type PG_Result_Counter is record
        PG_Result : PG_Result_Ptr;
        Count : Natural := 1;
        Active : Boolean := False;
    end record;
    type PG_Res_Counter_Ptr is access PG_Result_Counter;

    type Result is new Ada.Finalization.Controlled with record
        PG_Data : PG_Res_Counter_Ptr := null;
    end record;

    procedure Initialize(Object : in out Result);
    procedure Adjust(Object : in out Result);
    procedure Finalize(Object : in out Result);

    type Parameter is new Ada.Finalization.Controlled with record
        Value : Ada.Strings.Unbounded.String_Access;
    end record;
    procedure Adjust(Object : in out Parameter);
    procedure Finalize(Object : in out Parameter);

    Empty_Param_Array : constant Param_Array(1 .. 0) := (
        others => (Ada.Finalization.Controlled with Value => null));

    type Integer_Array is array(Natural range<>) of Integer;

    package Cstr renames Interfaces.C.Strings;

    function PQ_Connectdb(Connect_Info : Cstr.chars_ptr) return PG_Conn_Ptr;
    pragma Import(C, PQ_Connectdb, "PQconnectdb");

    function PQ_Error_Message(Conn : in PG_Conn_Ptr) return Cstr.chars_ptr;
    pragma Import(C, PQ_Error_Message, "PQerrorMessage");

    function PQ_Status(Conn : PG_Conn_Ptr) return Integer;
    pragma Import(C, PQ_Status, "PQstatus");

    function PQ_Exec_Params(
        Conn : PG_Conn_Ptr;
        Command : Cstr.chars_ptr;
        Num_Params : Integer;
        Param_Types : Cstr.chars_ptr;
        Param_Values : Cstr.chars_ptr_array;
        Param_Lengths : Integer_Array;
        Param_Formats : Integer_Array;
        Result_Format : Integer) return PG_Result_Ptr;
    pragma Import(C, PQ_Exec_Params, "PQexecParams");

    function PQ_Result_Status(Res : PG_Result_Ptr) return Integer;
    pragma Import(C, PQ_Result_Status, "PQresultStatus");

    function PQ_Get_Value(
        Res : PG_Result_Ptr;
        Row : Integer;
        Column : Integer) return Cstr.chars_ptr;
    pragma Import(C, PQ_Get_Value, "PQgetvalue");

    function PQ_Ntuples(Res : PG_Result_Ptr) return Integer;
    pragma Import(C, PQ_Ntuples, "PQntuples");

    function PQ_Nfields(Res : PG_Result_Ptr) return Integer;
    pragma Import(C, PQ_Nfields, "PQnfields");

    function PQ_Fname(
        Res : PG_Result_Ptr;
        Column_Number : Integer) return Cstr.chars_ptr;
    pragma Import(C, PQ_Fname, "PQfname");

    function PQ_Fnumber(
        Res : PG_Result_Ptr;
        Column_Name : Cstr.chars_ptr) return Integer;
    pragma Import(C, PQ_Fnumber, "PQfnumber");

    function PQ_Ftype(
        Res : PG_Result_Ptr;
        Column_Number : Integer) return Integer;
    pragma Import(C, PQ_Ftype, "PQftype");

    procedure PQ_Clear(Res : in PG_Result_Ptr);
    pragma Import(C, PQ_Clear, "PQclear");

    procedure PQ_Finish(Conn : in PG_Conn_Ptr);
    pragma Import(C, PQ_Finish, "PQfinish");
end MinPQ;
