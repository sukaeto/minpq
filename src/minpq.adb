------------------------------------------------------------------------------
--                                                                          --
--                        MINIMAL POSTGRESQL BINDING                        --
--                                                                          --
--                                M I N P Q                                 --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2009 Minerva Development Team                                  --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of a semi-thick binding to libPQ.  --
-- Not all functionality of libPQ is made available by this binding - it    --
-- started out as what was necessary for the operation of the MUD Server    --
-- Minerva.  Some other functionality has been added since to support other --
-- applications.                                                            --
--                                                                          --
------------------------------------------------------------------------------

with Interfaces.C; use Interfaces;
with Ada.Exceptions; use Ada.Exceptions;
with Ada.Strings.Fixed; use Ada.Strings.Fixed;
with Ada.Unchecked_Deallocation;
with Ada.Strings.Wide_Unbounded; use Ada.Strings.Wide_Unbounded;
with Ada.Strings.Wide_Fixed; use Ada.Strings.Wide_Fixed;
with Ada.Characters.Conversions; use Ada.Characters.Conversions;
with Ada.Strings.UTF_Encoding.Wide_Wide_Strings; use Ada.Strings.UTF_Encoding.Wide_Wide_Strings;
with Ada.Strings.UTF_Encoding.Wide_Strings; use Ada.Strings.UTF_Encoding.Wide_Strings;
with GNAT.Calendar.Time_IO;
with Ada.Strings;
with Ada.Wide_Wide_Characters.Unicode; use Ada.Wide_Wide_Characters.Unicode;

package body MinPQ is
    use type Interfaces.C.Strings.chars_ptr;
    use type Ada.Strings.Unbounded.String_Access;
    use Ada.Finalization;

    procedure Free_Counter is new
        Ada.Unchecked_Deallocation(PG_Connection_Counter, PG_Conn_Counter_Ptr);

    procedure Free_Counter is new
        Ada.Unchecked_Deallocation(PG_Result_Counter, PG_Res_Counter_Ptr);

    function Parameterize(Item : Wide_Wide_String) return Parameter is
    begin
        return Parameter'(Controlled with Value => new String'(Encode(Item)));
    end Parameterize;

    function Parameterize(Item : Wide_Wide_String) return Param_Array is
    begin
        return (1 => Parameterize(Item));
    end Parameterize;

    package body Integer_Type_IO is
        function Parameterize(Item : Num) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Num'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Num) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Integer_Type_IO;

    package body Modular_IO is
        function Parameterize(Item : Num) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Num'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Num) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Modular_IO;

    package body Floating_Point_IO is
        function Parameterize(Item : Num) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Num'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Num) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Floating_Point_IO;

    package body Fixed_IO is
        function Parameterize(Item : Num) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Num'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Num) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Fixed_IO;

    package body Decimal_IO is
        function Parameterize(Item : Num) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Num'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Num) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Num is
        begin
            return Num'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Decimal_IO;

    package body Enumeration_IO is
        function Parameterize(Item : Enum) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Enum'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Enum) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Enum is
        begin
            return Enum'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Enum is
        begin
            return Enum'Wide_Wide_Value(Res.Get(Row, Column));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Enumeration_IO;

    package body Boolean_IO is
        function Parameterize(Item : Boolean) return Parameter is
        begin
            return Parameter'(Controlled with Value => new String'(Boolean'Image(Item)));
        end Parameterize;

        function Parameterize(Item : Boolean) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        function Get(Res : Result; Row : Natural; Column : Natural) return Boolean is
        begin
            if Res.Get(Row, Column) = "t" then
                return True;
            elsif Res.Get(Row, Column) = "f" then
                return False;
            else
                raise Nonfatal_Query_Error with "Invalid Boolean Value";
            end if;
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Boolean is
        begin
            if Res.Get(Row, Column) = "t" then
                return True;
            elsif Res.Get(Row, Column) = "f" then
                return False;
            else
                raise Nonfatal_Query_Error with "Invalid Boolean Value";
            end if;
        end Get;
    end Boolean_IO;

    package body Time_IO is
        function Parameterize(Item : Ada.Calendar.Time) return Parameter is
        begin
            return Parameter'(Controlled with
                Value => new String'(
                    GNAT.Calendar.Time_IO.Image(Item, "%Y-%m-%d %H:%M:%S")));
        end Parameterize;

        function Parameterize(Item : Ada.Calendar.Time) return Param_Array is
        begin
            return (1 => Parameterize(Item));
        end Parameterize;

        -- also chops off fractional seconds in this implementation since
        -- GNAT.Calendar.Time_IO doesn't deal with those
        function Date_String_Without_Timezone(Date_String : String) return String is
            Last : Integer;
        begin
            Last := Index(Date_String, ".", Ada.Strings.Backward) - 1;

            if Last = -1 then
                Last := Index(Date_String, ":", Ada.Strings.Backward) - 1;

                if Last /= -1 then
                    Last := Index(Date_String, "-", Last + 1) - 1;
                end if;
            end if;

            if Last = -1 then
                return Date_String;
            else
                return Date_String(Date_String'First .. Last);
            end if;
        end Date_String_Without_Timezone;

        function Get(Res : Result; Row : Natural; Column : Natural) return Ada.Calendar.Time is
        begin
            return GNAT.Calendar.Time_IO.Value(
                Date_String_Without_Timezone(To_String(Res.Get(Row, Column))));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;

        function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Ada.Calendar.Time is
        begin
            return GNAT.Calendar.Time_IO.Value(
                Date_String_Without_Timezone(To_String(Res.Get(Row, Column))));
        exception when E : Constraint_Error =>
            raise Nonfatal_Query_Error with Exception_Message(E);
        end Get;
    end Time_IO;

    procedure Open(Conn : in out Connection; User : in Wide_String; Password : in Wide_String; Database : in Wide_String; Host : in String := ""; Port : in Natural := 5432) is
        Conn_String : Unbounded_Wide_String;
    begin
        Append(Conn_String, "user=" & User);
        Append(Conn_String, " password=" & Password);
        Append(Conn_String, " dbname=" & Database);
        Append(Conn_String, " client_encoding=UTF8");

        if Host /= "" then
            Append(Conn_String, " host=" & To_Wide_String(Host));
            Append(Conn_String, " port=" & Trim(Natural'Wide_Image(Port), Ada.Strings.Both));
        end if;
        Conn.Open(Encode(To_Wide_String(Conn_String)));
    end Open;

    procedure Open(Conn : in out Connection; Connect_String : in String) is
    begin
        Conn.Finalize;
        Conn.Initialize;

        Conn.Connect_String := new String'(Connect_String);
        Conn.PG_Data.PG_Conn := PQ_Connectdb(Cstr.New_String(Connect_String));

        if Conn.Status /= Connection_OK then
            raise Fatal_Connection_Error with Conn.Error_Message;
        end if;

        Conn.PG_Data.Active := True;
    end Open;

    function Is_Open(Conn : Connection) return Boolean is
    begin
        return Conn.PG_Data.Active;
    end Is_Open;

    function Clone(Conn : Connection) return Connection is
        New_Conn : Connection;
    begin
        if Conn.Is_Open then
            New_Conn.Open(Conn.Connect_String.all);
        end if;

        return New_Conn;
    end Clone;

    function "="(Left, Right : Connection) return Boolean is
    begin
        if Left.PG_Data = Right.PG_Data then
            return True;
        end if;

        if Left.Connect_String /= null and Right.Connect_String /= null then
            return Left.Connect_String.all = Right.Connect_String.all;
        end if;

        return False;
    end "=";

    function Same_Connection(Left, Right : Connection) return Boolean is
    begin
        return Left.PG_Data = Right.PG_Data;
    end Same_Connection;

    function Error_Message(Conn : in Connection) return String is
    begin
        return Cstr.Value(PQ_Error_Message(Conn.PG_Data.PG_Conn));
    end Error_Message;

    function Status(Conn : Connection) return Status_Type is
    begin
        case PQ_Status(Conn.PG_Data.PG_Conn) is
            when 0 => return Connection_OK;
            when 1 => return Connection_Bad;
            when others => return Unknown_Status;
        end case;
    end Status;

    function Status(Res : Result) return Exec_Status_Type is
    begin
        case PQ_Result_Status(Res.PG_Data.PG_Result) is
            when 0 => return Empty_Query;
            when 1 => return Command_OK;
            when 2 => return Tuples_OK;
            when 3 => return Copy_Out;
            when 4 => return Copy_In;
            when 5 => return Bad_Response;
            when 6 => return Nonfatal_Error;
            when 7 => return Fatal_Error;
            when others => return Unknown_Result_Status;
        end case;
    end Status;

    function Execute_Query(Conn : Connection'Class; Command : Wide_Wide_String; Parameters : Param_Array := Empty_Param_Array; Result_Format : Natural := 0) return Result is
        C_Params : Cstr.chars_ptr_array(C.size_t(1) .. C.size_t(Parameters'Length));
        -- These are only non-zero for binary parameters
        Param_Lengths : Integer_Array(1 .. Parameters'Length) := (others => 0);
        Param_Formats : Integer_Array(1 .. Parameters'Length) := (others => 0);
        New_Res : Result;
    begin
        if not Conn.PG_Data.Active then
            raise Fatal_Connection_Error with
                "Attempted to execute query on unopened connection";
        end if;

        for i in Parameters'Range loop
            C_Params(C.size_t(i - Parameters'First + 1)) := Cstr.New_String(
                Parameters(i).Value.all);
        end loop;

        New_Res.PG_Data.PG_Result :=
            PQ_Exec_Params(
                Conn.PG_Data.PG_Conn,
                Cstr.New_String(Encode(Command)),
                Parameters'Length,
                Cstr.Null_Ptr,
                C_Params,
                Param_Lengths,
                Param_Formats,
                Result_Format);

        case New_Res.Status is
            when Empty_Query =>
                raise Nonfatal_Query_Error with
                    "Execution was on Empty SQL Statement.";
            when Bad_Response =>
                raise Fatal_Query_Error with
                    "Server returned something unintelligible.";
            when Nonfatal_Error =>
                raise Nonfatal_Query_Error with Error_Message(Conn);
            when Fatal_Error =>
                raise Fatal_Query_Error with Error_Message(Conn);
            when Unknown_Result_Status =>
                raise Fatal_Query_Error with
                    "Unknown status.  This probably means there's a bug in MinPQ.";
            when others => -- The rest are 'good' statuses.
                New_Res.PG_Data.Active := True;
        end case;

        return New_Res;
    end Execute_Query;

    procedure Execute_Query(Conn : Connection; Command : Wide_Wide_String; Parameters : Param_Array := Empty_Param_Array) is
        New_Res : Result;
    begin
        New_Res := Execute_Query(Conn, Command, Parameters, 0);
    end Execute_Query;

    function Get(Res : Result; Row, Column : Natural) return Wide_Wide_String is
    begin
        if not Res.PG_Data.Active then
            raise Fatal_Query_Error with
                "Attempted to access an unused Query Result";
        elsif Res.Rows <= Row then
            raise Nonfatal_Query_Error with "Row index out of bounds";
        elsif Res.Columns <= Column then
            raise Nonfatal_Query_Error with "Column index out of bounds";
        end if;

        return Decode(
            Cstr.Value(PQ_Get_Value(Res.PG_Data.PG_Result, Row, Column)));
    end Get;

    function Get(Res : Result; Row : Natural; Column : Wide_Wide_String) return Wide_Wide_String is
        Raw : Cstr.chars_ptr;
    begin
        Raw := Cstr.New_String(Encode(Column));
        if not Res.PG_Data.Active then
            raise Fatal_Query_Error with
                "Attempted to access an unused Query Result";
        elsif Res.Rows <= Row then
            raise Nonfatal_Query_Error with "Row index out of bounds";
        elsif PQ_Fnumber(Res.PG_Data.PG_Result, Raw) = -1 then
            raise Nonfatal_Query_Error with "Non-existant Column name";
        end if;

        return Decode(
            Cstr.Value(PQ_Get_Value(
                Res.PG_Data.PG_Result,
                Row,
                PQ_Fnumber(Res.PG_Data.PG_Result, Raw))));
    end Get;

    function Rows(Res : Result) return Natural is
    begin
        return PQ_Ntuples(Res.PG_Data.PG_Result);
    end Rows;

    function Columns(Res : Result) return Natural is
    begin
        return PQ_Nfields(Res.PG_Data.PG_Result);
    end Columns;

    function Column_Name(Res : Result; Column : Natural) return Wide_Wide_String is
        Name : Cstr.chars_ptr;
    begin
        Name := PQ_Fname(Res.PG_Data.PG_Result, Column);

        if Name = Cstr.Null_Ptr then
            raise Nonfatal_Query_Error with "Column index out of bounds";
        end if;

        return Decode(Cstr.Value(Name));
    end Column_Name;

    function Column_Type(Res : Result; Column : Natural) return Data_Type is
        Col_Type : Integer;
    begin
        Col_Type := PQ_Ftype(Res.PG_Data.PG_Result, Column);
        case Col_Type is
            when 16 =>
                return SQL_Boolean;
            when 20 =>
                return SQL_Bigint;
            when 21 =>
                return SQL_Smallint;
            when 23 =>
                return SQL_Integer;
            when 700 =>
                return SQL_Real;
            when 701 =>
                return SQL_Double;
            when 1042 =>
                return SQL_Character;
            when 1043 =>
                return SQL_Varchar;
            when 1082 =>
                return SQL_Date;
            when 1083 =>
                return SQL_Time;
            when 1114 | 1184 => -- TIMESTAMPOID and TIMESTAMPTZOID
                return SQL_Timestamp;
            when 1700 =>
                return SQL_Decimal;
            when others =>
                return PG_Text;
        end case;
    end Column_Type;

    function Valid_Identifier(Identifier : Wide_Wide_String) return Boolean is
        function Valid_First_Char(Char : Wide_Wide_Character) return Boolean is
        begin
            return Is_Letter(Char) or Char = '_';
        end Valid_First_Char;

        function Valid_ID_Char(Char : Wide_Wide_Character) return Boolean is
        begin
            return Char = '$' or Is_Digit(Char) or Valid_First_Char(Char);
        end Valid_ID_Char;
    begin
        if not Valid_First_Char(Identifier(Identifier'First)) then
            return False;
        end if;

        for Index in Identifier'Range loop
            if not Valid_ID_Char(Identifier(Index)) then
                return False;
            end if;
        end loop;

        return True;
    end Valid_Identifier;

    procedure Close(Conn : in out Connection) is
    begin
        if Conn.PG_Data.Active then
            PQ_Finish(Conn.PG_Data.PG_Conn);
            Conn.PG_Data.Active := False;
        end if;
    end Close;

    procedure Initialize(Object : in out Connection) is
    begin
        Object.PG_Data := new PG_Connection_Counter;
    end Initialize;

    procedure Adjust(Object : in out Connection) is
    begin
        Object.PG_Data.Count := Object.PG_Data.Count + 1;
        if Object.Connect_String /= null then
            Object.Connect_String := new String'(Object.Connect_String.all);
        end if;
    end Adjust;

    procedure Finalize(Object : in out Connection) is
    begin
        Object.PG_Data.Count := Object.PG_Data.Count - 1;

        Ada.Strings.Unbounded.Free(Object.Connect_String);

        if Object.PG_Data.Count = 0 then
            Object.Close;
            Free_Counter(Object.PG_Data);
        end if;
    end Finalize;

    procedure Initialize(Object : in out Result) is
    begin
        Object.PG_Data := new PG_Result_Counter;
    end Initialize;

    procedure Adjust(Object : in out Result) is
    begin
        Object.PG_Data.Count := Object.PG_Data.Count + 1;
    end Adjust;

    procedure Finalize(Object : in out Result) is
    begin
        Object.PG_Data.Count := Object.PG_Data.Count - 1;

        if Object.PG_Data.Count = 0 then
            if Object.PG_Data.Active then
                PQ_Clear(Object.PG_Data.PG_Result);
            end if;

            Free_Counter(Object.PG_Data);
        end if;
    end Finalize;

    procedure Adjust(Object : in out Parameter) is
    begin
        if Object.Value /= null then
            Object.Value := new String'(Object.Value.all);
        end if;
    end Adjust;

    procedure Finalize(Object : in out Parameter) is
    begin
        Ada.Strings.Unbounded.Free(Object.Value);
    end Finalize;
end MinPQ;
