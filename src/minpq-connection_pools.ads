------------------------------------------------------------------------------
--                                                                          --
--                        MINIMAL POSTGRESQL BINDING                        --
--                                                                          --
--               M I N P Q . C O N N E C T I O N _ P O O L S                --
--                                                                          --
--                                 S p e c                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2012 Minerva Development Team                                  --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the specification for a pool of connections to a      --
-- database for use in a multi-tasking system.                              --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Containers.Vectors;

package MinPQ.Connection_Pools is
    package Connection_Vectors is new
        Ada.Containers.Vectors(Positive, Connection);

    -- The recommended way of using this connection pool type is to declare one
    -- immediately within a library package and to use the provided container
    -- type for obtaining connections.
    protected type Pool is
        -- Initializes the Pool to Size clones of Conn.
        procedure Initialize(Conn : in Connection; Size : in Positive);

        -- Sets the size of Pool to Size.  The side effects of this are: if
        -- Size is smaller than the current size of the pool, then N arbitrary
        -- connections will be ejected from the pool (where N is the difference
        -- between Size and the pool size.)  If Size is larger than the current
        -- size, then the difference between Size and the current size clones
        -- will be added to the pool.  If the pool has not been initialized, or
        -- Size is equal to the current size, then nothing happens.
        procedure Resize(Size : in Positive);

        -- Sets Conn to some available connection in the pool.  If no connection
        -- is available, this procedure will block until one becomes available.
        entry Obtain(Conn : out Connection);

        -- Sets Conn as available in the pool.  If Conn does not belong to the
        -- pool, nothing happens.
        procedure Release(Conn : in out Connection);
    private
        Connections : Connection_Vectors.Vector;
        Prototype : Connection;
    end Pool;

    -- A container's connection is automatically released back to the pool it
    -- came from when the container goes out of scope.
    type Container is tagged limited private;

    -- Obtains a connection from Connections for Self.  It goes without saying
    -- that this procedure can block.
    procedure Obtain(
        Self : in out Container;
        Connections : not null access Pool);

    -- Returns the connection associated with Self.  If Self's connection was
    -- not Obtained from a Connection Pool, this connection will be unopened.
    function Database(Self : Container) return MinPQ.Connection;

    -- If Self had previously obtained a connection from some connection pool,
    -- and that connection had not previously been released, then the
    -- connection will be released.  Otherwise, nothing happens.
    procedure Release(Self : in out Container);
private
    type Pool_Access is access all Pool;
    for Pool_Access'Storage_Size use 0;

    type Container is new Ada.Finalization.Limited_Controlled with record
        Connection_Pool : Pool_Access := null;
        Database : MinPQ.Connection;
    end record;

    overriding procedure Initialize(Self : in out Container) is null;
    overriding procedure Finalize(Self : in out Container);
end MinPQ.Connection_Pools;
