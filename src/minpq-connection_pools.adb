------------------------------------------------------------------------------
--                                                                          --
--                        MINIMAL POSTGRESQL BINDING                        --
--                                                                          --
--               M I N P Q . C O N N E C T I O N _ P O O L S                --
--                                                                          --
--                                 B o d y                                  --
--                                                                          --
------------------------------------------------------------------------------
-- Copyright 2012 Minerva Development Team                                  --
--                                                                          --
-- Licensed under the Apache License, Version 2.0 (the "License"); you may  --
-- not use this file except in compliance with the License. You may obtain  --
-- a copy of the License at                                                 --
--                                                                          --
--     http://www.apache.org/licenses/LICENSE-2.0                           --
--                                                                          --
-- Unless required by applicable law or agreed to in writing, software      --
-- distributed under the License is distributed on an "AS IS" BASIS,        --
-- WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. --
-- See the License for the specific language governing permissions and      --
-- limitations under the License.                                           --
--                                                                          --
------------------------------------------------------------------------------
-- This file contains the implementation of a pool of connections to a      --
-- database for use in a multi-tasking system.                              --
--                                                                          --
------------------------------------------------------------------------------

with Ada.Unchecked_Deallocation;

package body  MinPQ.Connection_Pools is
    procedure Free_Counter is new
        Ada.Unchecked_Deallocation(PG_Connection_Counter, PG_Conn_Counter_Ptr);

    protected body Pool is
        procedure Initialize(Conn : in Connection; Size : in Positive) is
        begin
            Connections.Set_Length(Ada.Containers.Count_Type(Size));
            for Index in Connections.First_Index .. Connections.Last_Index loop
                Connections.Replace_Element(Index, Conn.Clone);
            end loop;

            Prototype := Conn;
        end Initialize;

        procedure Resize(Size : in Positive) is
            Difference : Integer;
            use type Ada.Strings.Unbounded.String_Access;
        begin
            if Prototype.Connect_String = null then
                return;
            end if;

            Difference := Size - Positive(Connections.Length);

            if Difference < 0 then
                Difference := -Difference;
                Connections.Delete_Last(Ada.Containers.Count_Type(Difference));
            elsif  Difference > 0 then
                for I in 1 .. Difference loop
                    Connections.Append(Connections.First_Element.Clone);
                end loop;
            end if;
        end Resize;

        entry Obtain(Conn : out Connection) when not Connections.Is_Empty is
        begin
            Conn := Connections.First_Element;
            Connections.Delete_First;
        end Obtain;

        procedure Release(Conn : in out Connection) is
            New_Ref : Connection;
        begin
            if Conn = Prototype then
                Free_Counter(New_Ref.PG_Data);
                New_Ref.PG_Data := new PG_Connection_Counter;
                New_Ref.PG_Data.PG_Conn := Conn.PG_Data.PG_Conn;
                New_Ref.PG_Data.Active := True;

                New_Ref.Connect_String := new String'(Conn.Connect_String.all);

                Conn.PG_Data.PG_Conn := null;
                Conn.PG_Data.Active := False;

                Connections.Append(New_Ref);
            end if;
        end Release;
    end Pool;

    procedure Obtain(Self : in out Container; Connections : not null access Pool) is
    begin
        Connections.Obtain(Self.Database);
        Self.Connection_Pool := Pool_Access(Connections);
    end Obtain;

    function Database(Self : Container) return MinPQ.Connection is
    begin
        return Self.Database;
    end Database;

    procedure Release(Self : in out Container) is
    begin
        if Self.Connection_Pool /= null then
            Self.Connection_Pool.Release(Self.Database);
            Self.Connection_Pool := null;
        end if;
    end Release;

    overriding procedure Finalize(Self : in out Container) is
    begin
        Self.Release;
    end Finalize;
end MinPQ.Connection_Pools;
