OPT = -O2 -march=native -mfpmath=sse -msse3 -m64
DEBUG = -g -gnato -pg -gnata
PBASE = /home/sukaeto/good_stuff/sourceCode
PPATH = $(PBASE)/ahven-static

runtests: minpq
	ADA_PROJECT_PATH=ADA_PROJECT_PATH:$(PPATH) gnatmake $(DEBUG) -P unittests.gpr
	PGPASSFILE=./unit-tests/.pgpass psql -U unittester -d unittests -wqf ./unit-tests/prepare-db.sql
	cd unit-tests && ./tests
debug: minpq-debug
	ADA_PROJECT_PATH=ADA_PROJECT_PATH:$(PPATH) gnatmake $(DEBUG) -P unittests.gpr
	PGPASSFILE=./unit-tests/.pgpass psql -U unittester -d unittests -wqf ./unit-tests/prepare-db.sql
	cd unit-tests && ./tests
minpq: makedirs
	ADA_PROJECT_PATH=ADA_PROJECT_PATH:$(PPATH) gnatmake $(OPT) -P minpq.gpr
minpq-debug: makedirs
	ADA_PROJECT_PATH=ADA_PROJECT_PATH:$(PPATH) gnatmake $(DEBUG) -P minpq.gpr
makedirs:
	test -d lib || mkdir lib
	test -d obj || mkdir obj
clean:
	rm obj/*
	rm lib/*
	rm unit-tests/tests
