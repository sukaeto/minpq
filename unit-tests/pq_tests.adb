with Ahven; use Ahven;
with MinPQ;
with MinPQ.Integer_IO;
with MinPQ.Connection_Pools;
with Ada.Characters.Conversions; use Ada.Characters.Conversions;
with Ada.Calendar; use Ada.Calendar;
with GNAT.Calendar.Time_IO;

package body PQ_Tests is
    use type MinPQ.Status_Type;
    use type MinPQ.Exec_Status_Type;
    use type MinPQ.Data_Type;

    procedure Initialize (T : in out Test) is
    begin
        T.Set_Name("PQ Binding Tests");

        T.Add_Test_Routine(
            Test_Basic_Database_Functionality'Access,
            "Test basic connection, etc. functionality");

        T.Add_Test_Routine(
            Test_Column_Name_Type'Access,
            "Test Column Name and Type functions");

        T.Add_Test_Routine(
            Test_Fixed_Point_IO'Access,
            "Test Insert/retrieve Fixed/Decimal Fixed values");

        T.Add_Test_Routine(
            Test_Modular_IO'Access,
            "Test Insert/retrieve of Modular values");

        T.Add_Test_Routine(
            Test_Enumeration_IO'Access,
            "Test Insert/retrieve of Enumeration values");

        T.Add_Test_Routine(
            Test_Time_IO'Access,
            "Test Insert/retrieve of Time values");

        T.Add_Test_Routine(
            Test_Valid_Identifier'Access,
            "Test Table/Column name validator");

        T.Add_Test_Routine(
            Test_Pools'Access,
            "Test Basic Connection Pools functionality");
    end Initialize;

    procedure Assert_Integer_Equal is new
        Ahven.Assert_Equal(Integer, Integer'Image);

    procedure Assert_Data_Type_Equal is new
        Ahven.Assert_Equal(MinPQ.Data_Type, MinPQ.Data_Type'Image);

    procedure Test_Basic_Database_Functionality is
        D1 : MinPQ.Connection;
        Res : MinPQ.Result;
        Str : Wide_Wide_String(1 .. 1);
    begin
        D1.Open("unittester", "unittester", "unittests");

        Assert(
            D1.Status = MinPQ.Connection_OK,
            "Connection Status Not OK: " & MinPQ.Status_Type'Image(D1.Status));

        Res := D1.Execute_Query("CREATE TABLE Test_Table (Num  Integer, Even Boolean);");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        for i in 0 .. 100 loop
            Res := D1.Execute_Query(
                "INSERT INTO Test_Table VALUES ($1, $2);",
                (MinPQ.Integer_IO.Parameterize(i),
                 MinPQ.Boolean_IO.Parameterize(i mod 2 = 0)));
            Assert(
                Res.Status =  MinPQ.Command_OK,
                "Insert into Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));
        end loop;

        Res := D1.Execute_Query("SELECT * FROM Test_Table ORDER BY Num");
        Assert_Integer_Equal(Res.Rows, 101, "");
        Assert_Integer_Equal(Res.Columns, 2, "");
        for i in 0 .. 100 loop
            Str := Res.Get(i, "Even");
            if i mod 2 = 0 then
                Assert(Str = "t", "Bad Result in Test Table Query.");
                Assert(MinPQ.Boolean_IO.Get(Res, i, "Even"), "Bad Result in Test Table Query.");
            else
                Assert(Str = "f", "Bad Result in Test Table Query.");
                Assert(not MinPQ.Boolean_IO.Get(Res, i, "Even"), "Bad Result in Test Table Query.");
            end if;
            Assert_Integer_Equal(
                i,
                MinPQ.Integer_IO.Get(Res, i, "Num"),
                "Bad Result in Test Table Query.");
        end loop;

        D1.Close;
    end Test_Basic_Database_Functionality;

    procedure Test_Column_Name_Type is
        D1 : MinPQ.Connection;
        Res : MinPQ.Result;
    begin
        D1.Open("unittester", "unittester", "unittests");

        Res := D1.Execute_Query("CREATE TABLE Test_Column_Funcs (Num Integer, Bool Boolean, Str Text, Bigreal Float8, Smallnum Smallint, Bignum Bigint, Smallreal Float4, Fixednum Decimal, Fixedstr char(5), Varstr varchar(5), Sometime Time, Somedate Date, Sometime2 Timestamp);");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("INSERT INTO Test_Column_Funcs VALUES (10, True, 'meh', 9.9, 10, 10, 9.9, 9.9, 'meh12', 'meh', '8:00:00', '2012-07-19', '2012-07-19T08:00:00');");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT * FROM Test_Column_Funcs;");

        Assert(Res.Column_Name(0) = "num", "Assertion Failed: Expected ""num"", Got """ & To_String(Res.Column_Name(0)) & """");
        Assert(Res.Column_Name(1) = "bool", "Assertion Failed: Expected ""bool"", Got """ & To_String(Res.Column_Name(1)) & """");
        Assert(Res.Column_Name(2) = "str", "Assertion Failed: Expected ""str"", Got """ & To_String(Res.Column_Name(2)) & """");
        Assert(Res.Column_Name(3) = "bigreal", "Assertion Failed: Expected ""bigreal"", Got """ & To_String(Res.Column_Name(3)) & """");

        Assert_Data_Type_Equal(Res.Column_Type(0), MinPQ.SQL_Integer, "");
        Assert_Data_Type_Equal(Res.Column_Type(1), MinPQ.SQL_Boolean, "");
        Assert_Data_Type_Equal(Res.Column_Type(2), MinPQ.PG_Text, "");
        Assert_Data_Type_Equal(Res.Column_Type(3), MinPQ.SQL_Double, "");

        Assert_Data_Type_Equal(Res.Column_Type(4), MinPQ.SQL_Smallint, "");
        Assert_Data_Type_Equal(Res.Column_Type(5), MinPQ.SQL_Bigint, "");
        Assert_Data_Type_Equal(Res.Column_Type(6), MinPQ.SQL_Real, "");
        Assert_Data_Type_Equal(Res.Column_Type(7), MinPQ.SQL_Decimal, "");

        Assert_Data_Type_Equal(Res.Column_Type(8), MinPQ.SQL_Character, "");
        Assert_Data_Type_Equal(Res.Column_Type(9), MinPQ.SQL_Varchar, "");
        Assert_Data_Type_Equal(Res.Column_Type(10), MinPQ.SQL_Time, "");
        Assert_Data_Type_Equal(Res.Column_Type(11), MinPQ.SQL_Date, "");
        Assert_Data_Type_Equal(Res.Column_Type(12), MinPQ.SQL_Timestamp, "");

        D1.Close;
    end Test_Column_Name_Type;

    procedure Test_Fixed_Point_IO is
        D1 : MinPQ.Connection;
        Res : MinPQ.Result;

        type Test_Fixed is delta 0.0000001 range 0.0 .. 1000.0;
        for Test_Fixed'Small use 0.0000001;

        type Test_Decimal is delta 0.0001 digits 18;

        F1 : Test_Fixed := 923.8243143;
        F2 : Test_Decimal := 928734832.6534;

        package Test_Fixed_PQ_IO is new MinPQ.Fixed_IO(Test_Fixed);
        package Test_Decimal_PQ_IO is new MinPQ.Decimal_IO(Test_Decimal);

        procedure Assert_Fixed_Equal is new
            Ahven.Assert_Equal(Test_Fixed, Test_Fixed'Image);

        procedure Assert_Decimal_Equal is new
            Ahven.Assert_Equal(Test_Decimal, Test_Decimal'Image);
    begin
        D1.Open("unittester", "unittester", "unittests");

        Res := D1.Execute_Query("CREATE TABLE Test_Fixed_IO (Dummy Integer, Num Numeric(38, 10));");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query(
            "INSERT INTO Test_Fixed_IO VALUES (0, $1);",
            Test_Fixed_PQ_IO.Parameterize(F1));

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT Num FROM Test_Fixed_IO WHERE Dummy = 0;");

        Assert_Fixed_Equal(F1, Test_Fixed_PQ_IO.Get(Res, 0, 0), "Fixed point I/O correctness");

        Res := D1.Execute_Query(
            "INSERT INTO Test_Fixed_IO VALUES (1, $1);",
            Test_Decimal_PQ_IO.Parameterize(F2));

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT Num FROM Test_Fixed_IO WHERE Dummy = 1;");

        Assert_Decimal_Equal(F2, Test_Decimal_PQ_IO.Get(Res, 0, 0), "Decimal Fixed point I/O correctness");
    end Test_Fixed_Point_IO;

    procedure Test_Modular_IO is
        D1 : MinPQ.Connection;
        Res : MinPQ.Result;

        type Test_Modular is mod 2**32;
        package Test_Modular_PQ_IO is new MinPQ.Modular_IO(Test_Modular);
        procedure Assert_Modular_Equal is new
            Ahven.Assert_Equal(Test_Modular, Test_Modular'Image);
    begin
        D1.Open("unittester", "unittester", "unittests");

        Res := D1.Execute_Query("CREATE TABLE Test_Modular_IO (Dummy Integer, Num Bigint);");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query(
            "INSERT INTO Test_Modular_IO VALUES (0, $1);",
            Test_Modular_PQ_IO.Parameterize(875253994));

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT Num FROM Test_Modular_IO WHERE Dummy = 0;");

        Assert_Modular_Equal(Test_Modular_PQ_IO.Get(Res, 0, 0), 875253994, "Modular I/O correctness");
    end Test_Modular_IO;

    procedure Test_Enumeration_IO is
        D1 : MinPQ.Connection;
        Res : MinPQ.Result;

        type Test_Colors is (Red, Blue, Green, Yellow, Orange, Purple);
        package Test_Colors_PQ_IO is new MinPQ.Enumeration_IO(Test_Colors);
        procedure Assert_Colors_Equal is new
            Ahven.Assert_Equal(Test_Colors, Test_Colors'Image);
    begin
        D1.Open("unittester", "unittester", "unittests");

        Res := D1.Execute_Query("CREATE TABLE Test_Enumeration_IO (Dummy Integer, Color Text);");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query(
            "INSERT INTO Test_Enumeration_IO VALUES (0, $1);",
            Test_Colors_PQ_IO.Parameterize(Green));

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT Color FROM Test_Enumeration_IO WHERE Dummy = 0;");

        Assert_Colors_Equal(Test_Colors_PQ_IO.Get(Res, 0, 0), Green, "Enumeration I/O correctness");
    end Test_Enumeration_IO;

    procedure Test_Time_IO is
        function ISO_Timestamp_Image(Timestamp : Time) return String is
        begin
            return GNAT.Calendar.Time_IO.Image(Timestamp, "%Y-%m-%d %H:%M:%S");
        end ISO_Timestamp_Image;

        procedure Assert_Time_Equal is new
            Ahven.Assert_Equal(Time, ISO_Timestamp_Image);

        D1 : MinPQ.Connection;
        Res : MinPQ.Result;
        T1 : Time;
    begin
        D1.Open("unittester", "unittester", "unittests");

        Res := D1.Execute_Query("CREATE TABLE Test_Time_IO (Dummy Integer, Test_Time Timestamp with time zone);");

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Create Test Table Status Not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        T1 := Clock;
        T1 := Time_Of(
            Year(Clock),
            Month(Clock),
            Day(Clock),
            -- Time_IO currently only supports second-level accuracy.
            Duration(Long_Float'Floor(Long_Float(Seconds(Clock)))));

        Res := D1.Execute_Query(
            "INSERT INTO Test_Time_IO VALUES (0, $1);",
            MinPQ.Time_IO.Parameterize(T1));

        Assert(
            Res.Status =  MinPQ.Command_OK,
            "Insert test row into table not OK: " & MinPQ.Exec_Status_Type'Image(Res.Status));

        Res := D1.Execute_Query("SELECT Test_Time FROM Test_Time_IO WHERE Dummy = 0;");

        Assert_Time_Equal(MinPQ.Time_IO.Get(Res, 0, 0), T1, "Time I/O correctness");
    end Test_Time_IO;

    procedure Test_Valid_Identifier is
    begin
        Assert(MinPQ.Valid_Identifier("Some_Table"), "Typical Identifier is valid");
        Assert(MinPQ.Valid_Identifier("Some9T4ble"), "Identifier with numbers is valid");
        Assert(MinPQ.Valid_Identifier("Some$Table"), "Identifier with a dollar sign is valid");
        Assert(MinPQ.Valid_Identifier("_Some_Table"), "Identifier staring with underscore is valid");
        Assert(MinPQ.Valid_Identifier("某些表格"), "Identifier with non-latin 'letter' characters is valid");
        Assert(not MinPQ.Valid_Identifier("4SomeTable"), "Identifier starting with a number is invalid");
        Assert(not MinPQ.Valid_Identifier("X#%^()☺"), "Identifier with a bunch of bad characters is invalid");

        -- Only Letters, digits and underscores are valid in Identifiers, but
        -- these are characters known to be used to cause shenanigans.
        Assert(not MinPQ.Valid_Identifier("Some_Tab'l"), "Identifier with a quote is invalid");
        Assert(not MinPQ.Valid_Identifier("Some_Tab;l"), "Identifier with a semicolon is invalid");
        Assert(not MinPQ.Valid_Identifier("Some_Tab,l"), "Identifier with a comma is invalid");
    end Test_Valid_Identifier;

    procedure Test_Pools is
        --D1, D2, D3, D4 : MinPQ.Connection;
        D1 : MinPQ.Connection;
        Pool : aliased MinPQ.Connection_Pools.Pool;
        use type MinPQ.Connection;
    begin
        D1.Open("unittester", "unittester", "unittests");

        Pool.Initialize(D1, 3);

        declare
            D2, D3, D4 : MinPQ.Connection_Pools.Container;
        begin
            D2.Obtain(Pool'Unchecked_Access);
            D3.Obtain(Pool'Unchecked_Access);

            Assert(D2.Database = D3.Database, "Pool connetions are to the same database");
            Assert(
                not MinPQ.Same_Connection(D2.Database, D3.Database),
                "Pool connetions are not the same connection");

            Assert(D2.Database.Is_Open, "Obtained connection is open");
            D2.Release;
            Assert(not D2.Database.Is_Open, "Releasing a database breaks its reference");

            -- This check passes if these don't cause the test to block
            D2.Obtain(Pool'Unchecked_Access);
            D4.Obtain(Pool'Unchecked_Access);
        end;

        D1.Close;
    end Test_Pools;
end PQ_Tests;
