with Ahven.Framework;

package PQ_Tests is
    type Test is new Ahven.Framework.Test_Case with null record;

    procedure Initialize (T : in out Test);

    procedure Test_Basic_Database_Functionality;

    procedure Test_Column_Name_Type;

    procedure Test_Fixed_Point_IO;

    procedure Test_Modular_IO;

    procedure Test_Enumeration_IO;

    procedure Test_Time_IO;

    procedure Test_Valid_Identifier;

    procedure Test_Pools;
end PQ_Tests;
