with Ahven.Text_Runner;
with Ahven.Framework;
with PQ_Tests;

procedure Tests is
    S : Ahven.Framework.Test_Suite :=
        Ahven.Framework.Create_Suite("All MinPQ Tests");
begin
    S.Add_Test(new PQ_Tests.Test);
    Ahven.Text_Runner.Run(S);
end Tests;
